function changeActive(e) {
  const tab = e.target;
  console.log(tab.classList)
  if ([...tab.classList].includes('uk-active')) {
    tab.classList.remove('uk-active');
  } else {
    tab.classList.add('uk-active');
  }
}

function navClick() {
  const nav = document.getElementById('nav');
  nav.addEventListener('click', changeActive);
}

document.addEventListener('DOMContentLoaded', navClick);
