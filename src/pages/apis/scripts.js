const apiRequest = async (url, callback) =>
  callback(await fetch(url).then((r) => r.json()));

class ApiData {
  constructor(name, url) {
    this.name = name;
    this.url = url;
  }
}

const insertTable = (title, data) => {
  const app = document.getElementById('app');
  let html = `
    <h3 class="uk-margin">${title}</h2>
    <ul class="uk-grid-small uk-child-width-1-2 uk-child-width-1-4@s uk-text-center" uk-sortable="handle: .uk-card" uk-grid>
    `;
  data.forEach((element) => {
    html += `
      <li>
        <div class="uk-card uk-card-default uk-card-body">${element}</div>
      </li>
    `;
  });
  app.innerHTML += html + '</ul>';
};

const loadData = () => {
  apiRequest('https://restcountries.eu/rest/v2/all', (response) => {
    insertTable(
      'Countries',
      response.slice(0, 8).map((country) => country.name)
    );
  });

  apiRequest('https://ghibliapi.herokuapp.com/films', (response) => {
    insertTable(
      'Ghibli Films',
      response.slice(0, 8).map((film) => film.title)
    );
  });

  apiRequest('http://universities.hipolabs.com/search?name=middle', (response) => {
    insertTable(
      'University',
      response.slice(0, 8).map((history) => history.name)
    );
  });

  apiRequest('https://binaryjazz.us/wp-json/genrenator/v1/genre/8', (response) => {
    insertTable('Random Genres', response)
  });

  apiRequest('https://binaryjazz.us/wp-json/genrenator/v1/story/8', (response) => {
    insertTable('Random Phrases', response)
  });
};
document.addEventListener('DOMContentLoaded', loadData);
