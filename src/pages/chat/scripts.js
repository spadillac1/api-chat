let socket = io();
let author = '';

const insertMessage = (author, text) => {
  const chatContent = document.getElementById('chatContent');
  chatContent.innerHTML += `
    <div class="message">
      <h4 class="uk-margin-small">${author}</h4>
      <p class="uk-margin-small">${text}</p>
    </div>
  `;
};

const insertUser = (name) => {
  const userList = document.getElementById('userList');
  userList.innerHTML += `<li>${name}</li>`;
};

socket.on('chat:message', (data) => {
  insertMessage(data.author, data.message);
});

socket.on('chat:connect', (socket) => {
  insertUser(socket.author);
})

document.addEventListener('keyup', (e) => {
  const message = document.getElementById('textInput').value;
  if ((e.key === 'Enter' || e.keyCode === 13) && message) {
    socket.emit('chat:message', { author, message });
  }
});

document.addEventListener('DOMContentLoaded', () => {
  author = prompt('Author Name');
  socket.emit('chat:connect', {author});
});
