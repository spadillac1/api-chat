const path = require('path');
const express = require('express');
const socket = require('socket.io');
const app = express();

app.set('port', process.env.PORT || 3000);
app.use(express.static(path.join(__dirname, 'src')));
const server = app.listen(app.get('port'), () => {
  console.log('server on ', app.get('port'));
});

const io = socket(server);
io.on('connection', (socket) => {
  socket.on('chat:message', (data) => {
    io.sockets.emit('chat:message', data);
  });

  socket.on('chat:connect', (author) => {
    io.sockets.emit('chat:connect', author);
  });
});
